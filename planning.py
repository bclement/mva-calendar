import os
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, render_template, g, session, redirect, request, url_for, flash

app = Flask(__name__)

app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'planning.db'),
    DEBUG=True,
    SECRET_KEY='Dev key',
))

def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv


def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

def render(*args, **kwargs):
    if 'email' in session:
        kwargs['student'] = { 'email': session['email'], 'alerts': session['alerts'] }
    return render_template(*args, **kwargs)

@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()

@app.route('/', methods=['GET'])
def index():
    calendar = request.args.get('calendar', None)
    cset = set()
    calendar_url = None
    if 'email' in session:
        calendar_url = url_for('student_calendar', sid=session['id'], _external=True)
        cur = get_db().execute('select id from course inner join student_courses on student_courses.course = course.id where student_courses.student = ?', [session['id']])
        for row in cur.fetchall():
            cset.add(int(row['id']))
    elif calendar is not None:
        calendar = int(calendar)
        calendar_url = url_for('get_calendar', cset=calendar, _external=True)
        nb = 0
        while calendar > 0:
            if calendar & 1:
                cset.add(nb)
            calendar >>= 1
            nb += 1
    cur = get_db().execute('select id, name, url, teachers, category from course order by name asc')
    return render('index.html', courses=cur.fetchall(), selected=cset, calendar_url=calendar_url)

import re

COURSE_RE = re.compile(r'^courses\[([0-9]+)\]$')

@app.route('/', methods=['POST'])
def update_calendar():
    cset = set()
    for name in request.form:
        m = COURSE_RE.match(name)
        if m is not None:
            cset.add(int(m.group(1)))
    if 'email' in request.form and 'account' in request.form:
        student = query_db('select id, alert from student where email = ?', [request.form['email']], one=True)
        if student is None:
            db = get_db()
            cur = db.execute('insert into student (email, alert) values (?, ?)', [request.form['email'], 0])
            session['id'] = cur.lastrowid
            session['email'] = request.form['email']
            session['alerts'] = 0
        else:
            session['email'] = request.form['email']
            session['alerts'] = student['alert']
            session['id'] = student['id']
    if 'email' in session:
        email = session['email']
        db = get_db()
        db.execute('delete from student_courses where student = ?', [session['id']])
        db.executemany('insert into student_courses (student, course) values (?, ?)',
            [(session['id'], course) for course in cset])
        db.commit()
        return redirect(url_for('index'))
    else:
        cset_as_int = 0
        for i in cset:
            cset_as_int |= 1 << i
        return redirect(url_for('index', calendar=cset_as_int))

@app.route('/login', methods=['POST'])
def login():
    email = request.form['email']
    student = query_db('select id, alert from student where email = ?', [email], one=True)
    if student is not None:
        session['email'] = email
        session['alerts'] = student['alert']
        session['id'] = student['id']
    else:
        flash('There is no account associated with this email address.')
    return redirect(url_for('index'))

@app.route('/alerts', methods=['POST'])
def toggle_alerts():
    if 'email' not in session:
        return redirect(url_for('index'))
    if 'disable' in request.form:
        nvalue = 0
    elif 'enable' in request.form:
        nvalue = 1
    else:
        return redirect(url_for('index'))

    db = get_db()
    db.execute('update student set alert = ? where email = ?', [nvalue, session['email']])
    db.commit()
    session['alerts'] = nvalue
    return redirect(url_for('index'))

@app.route('/logout')
def logout():
    session.pop('email', None)
    session.pop('alerts', None)
    session.pop('id', None)
    return redirect(url_for('index'))

@app.route('/2015/semester-1/courses/<int:cset>.ics', methods=['GET'])
def get_calendar(cset):
    return 'a calendar'

@app.route('/student/<int:sid>/calendar/2015/semester-1.ics', methods=['GET'])
def student_calendar(sid):
    return 'a calendar'

if __name__ == '__main__':
    app.run()
