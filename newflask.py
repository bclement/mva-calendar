import os
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, g, make_response, render_template, redirect, request

app = Flask(__name__)

app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'planning.db'),
    DEBUG=True,
    SECRET_KEY='Dev key',
))


def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv


def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


@app.get('/')
def show_courses():
    db = get_db()
    cur = db.execute('select id, name from course')
    return render_template('form.html', courses=cur.fetchall())


@app.post('/')
def get_courses():
    data = request.form
    cset = 0
    for i in data['courseID']:
        cset &= 1 << (int(i) - 1)
    return redirect(str(cset) + '.ics')


@app.get('/<int:cset>.ics')
def get_calendar(cset):
    db = get_db()
    cur = db.execute('select id from course')
    ids = set()
    for course in cur.fetchall():
        if (1 << (course.id - 1)) & cset:
            ids.add(course.id)
    cur = db.execute('select ' +
                     'lesson.date AS date, ' +
                     'event.start AS start, ' +
                     'event.end AS end, ' +
                     'course.name AS name, ' +
                     'event.location AS location, ' +
                     'course.url AS url, ' +
                     'lesson.uuid AS uuid ' +
                     'from lesson ' +
                     'inner join event on event.id = lesson.event ' +
                     'inner join course on course.id = lesson.course ' +
                     'where lesson.course in ?', ids)
    resp = make_response(create_calendar(cur.fetchall()).to_ical())
    resp.content_type = "text/calendar"
    return resp
