import re
import dateparser
from datetime import timedelta
import hashlib

import fileinput

from ngram import NGram

course_info = {
    'Introduction to medical image analysis': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/introduction-to-medical-image-analysis-218869.kjsp',
        'teachers': 'H. DELINGETTE, X. PENNEC',
    },
    'Introduction à l\'imagerie numérique': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/introduction-a-l-imagerie-numerique-222029.kjsp',
        'teachers': 'J. DELON,Y. GOUSSEAU',
    },
    'Optimization - Applications to image processing': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/optimization-applications-to-image-processing-164654.kjsp',
        'teachers': 'S. LADJAL, M. NIKOLOVA',
    },
    'Object recognition and computer vision': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/object-recognition-and-computer-vision-221990.kjsp',
        'teachers': 'I. LAPTEV, J. PONCE, C. SCHMID, J. SIVIC',
    },
    'Sub-pixel image processing': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/sub-pixel-image-processing-219041.kjsp',
        'teachers': 'L. MOISAN',
    },
    '3D computer vision': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/3d-computer-vision-214777.kjsp',
        'teachers': 'R. MARLET, P. MONASSE',
    },
    'The PDEs of image processing and their surprising applications': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/the-partial-differential-equations-of-image-processing-and-their-surprising-applications--214255.kjsp',
        'teachers': 'J.-M. MOREL',
    },
    'Convex optimization and applications in machine learning': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/convex-optimization-and-applications-in-machine-learning-161994.kjsp',
        'teachers': 'A. D\'ASPREMONT',
    },
    'Probabilistic graphical models': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/probabilistic-graphical-models-214257.kjsp',
        'teachers': 'F. BACH, S. LACOSTE-JULIEN, G. OBOZINSKI',
    },
    'Introduction to Deep Learning': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/introduction-to-deep-learning-165822.kjsp',
        'teachers': 'I. KOKKINOS',
    },
    'Reinforcement learning': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/reinforcement-learning-214281.kjsp',
        'teachers': 'A. LAZARIC',
    },
    'Sparse wavelet representations and classification': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/sparse-wavelet-representations-and-classification--222028.kjsp',
        'teachers': 'S. MALLAT',
    },
    'Sparsity and compressed sensing': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/sparsity-and-compressed-sensing-214256.kjsp',
        'teachers': 'G. PEYRE',
    },
    'Méthodes mathématiques pour les neurosciences': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/methodes-mathematiques-pour-les-neurosciences-162073.kjsp',
        'teachers': 'E. TANRE, R. VELTZ',
    },
    'Graphs in machine learning': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/graphs-in-machine-learning-267194.kjsp',
        'teachers': 'M. VALKO',
    },
    'Introduction to statistical learning': {
        'url': 'http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/introduction-to-statistical-learning-162015.kjsp',
        'teachers': 'N. VAYATIS',
    },
}

course_map = NGram(course_info)

dp = dateparser.DateDataParser(['fr'])


def read_planning(lines):
    weeks = []
    week = []
    for line in lines:
        if line[0] == '\f':
            if week[0].lower().startswith('semaine'):
                week = week[1:]
            weeks.append(week)
            week = [line[1:-1]]
        else:
            week.append(line[:-1])
    if len(week) != 1:
        raise Exception("Format invalide; le fichier doit finir par un \\f")
    return weeks[1:]

SENTENCE = re.compile(r'(?:^|  )((?:[^ ]+ )*[^ ]+)(?=  |$)')


def extract_columns(week):
    header = week[0]
    dayspans = []
    for match in SENTENCE.finditer(header):
        dayspans.append(match.span(1))

    if len(dayspans) != 5:
        raise Exception("Format invalide; 5 jours par semaine")

    maxsize = max(len(line) for line in week)
    limits = [[0, maxsize] for _ in dayspans]
    for i, (start, end) in enumerate(dayspans):
        if i > 0:
            limits[i - 1][1] = start
        if i + 1 < len(limits):
            limits[i + 1][0] = end

    return limits


def extract_sentence(line, start, end):
    best = None
    source = line[start:end]
    for match in SENTENCE.finditer(source):
        if best is None:
            best = match.span(1)
        else:
            cstart, cend = match.span(1)
            if cend - cstart > best[1] - best[0]:
                best = (cstart, cend)
    if best is None:
        return ''
    if start + best[0] > 0 and line[start + best[0] - 1] != ' ':
        return ''
    if start + best[1] < len(line) and line[start + best[1]] != ' ':
        return ''
    if best[1] < len(source) / 4.:
        return ''
    if best[0] > len(source) * 3. / 4.:
        return ''
    return source[best[0]:best[1]].strip()


def split_days(week):
    limits = extract_columns(week)
    days = [
        [extract_sentence(line, start, end) for line in week[1:]]
        for (start, end) in limits
    ]
    return {
        dp.get_date_data(week[0][s:e])['date_obj']: [
            course
            for course in courses
            if course not in ['', 'FERIE', 'VACANCES']
        ]
        for courses, (s, e) in zip(days, limits)
    }

HOURS = re.compile(r'^([0-9]{1,2}h[0-9]{2}) - ([0-9]{2}h[0-9]{2})$')


def iter_courses(courses):
    start = None
    end = None
    course = None
    for line in courses:
        m = HOURS.match(line)
        if m is None:
            if course is None:
                raise Exception("Invalid format")
            course.append(line)
        else:
            if course is not None:
                yield ((start, end), course)
            start = [int(x) for x in m.group(1).split('h')]
            start = timedelta(**dict(zip(['hours', 'minutes'], start)))
            end = [int(x) for x in m.group(2).split('h')]
            end = timedelta(**dict(zip(['hours', 'minutes'], end)))
            course = []
    if course is not None:
        yield ((start, end), course)


def analyze_week(week):
    for date, courses in split_days(week).items():
        for (start, end), course in iter_courses(courses):
            location = course[-1]
            if 'Delingette' in location:
                location = ''
                name = ' '.join(course[0:-1])
            else:
                name = ' '.join(course[0:-2])
            is_tp = name.startswith('TP')
            was = name
            name = course_map.find(name)
            if name is None:
                raise Exception("Cours inconnu: " + was)
            uid = hashlib.md5()
            uid.update(repr(date).encode())
            uid.update(str(is_tp).encode())
            uid.update(name.encode())
            yield (uid.hexdigest(), {
                'name': name,
                'tp': is_tp,
                'date': str(date.date()),
                'start': str(start),
                'end': str(end),
                'location': location
            })


def build_planning():
    weeks = read_planning(fileinput.input())

    courses = [course for week in weeks for course in analyze_week(week)]
    mapping = {k: v for k, v in courses}
    if len(courses) != len(mapping):
        raise Exception("Hash error")

    return mapping


if __name__ == '__main__':
    from sqlite3 import dbapi2 as sqlite3
    db = sqlite3.connect('planning.db')

    for k, v in course_info.items():
        db.execute('insert into course (name, url, teachers) values (?, ?, ?)',
                   [k, v['url'], v['teachers']])
    
    planning = build_planning()
    for k, course in planning.items():
        cur = db.execute('insert into event (version, start, end) values (?, ?, ?)',
                         [1, course['start'], course['end']])
        db.execute('insert into lesson (course, date, tp, uuid, event) ' +
                   'select id, ?, ?, ?, ? from course where name = ?',
                   [course['date'], course['tp'], k, cur.lastrowid, course['name']])
        planning[k] = { 'version': 1, 'value': planning[k] }
    db.commit()
