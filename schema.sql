PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;

CREATE TABLE course (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name VARCHAR,
  url VARCHAR,
  teachers VARCHAR,
  category CHAR(5)
);

INSERT INTO "course" VALUES(1,'Probabilistic graphical models','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/probabilistic-graphical-models-214257.kjsp','F. BACH, S. LACOSTE-JULIEN, G. OBOZINSKI', 'learn');
INSERT INTO "course" VALUES(2,'Sparse wavelet representations and classification','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/sparse-wavelet-representations-and-classification--222028.kjsp','S. MALLAT', 'learn');
INSERT INTO "course" VALUES(3,'Méthodes mathématiques pour les neurosciences','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/methodes-mathematiques-pour-les-neurosciences-162073.kjsp','E. TANRE, R. VELTZ', 'learn');
INSERT INTO "course" VALUES(4,'Introduction to statistical learning','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/introduction-to-statistical-learning-162015.kjsp','N. VAYATIS', 'learn');
INSERT INTO "course" VALUES(5,'The PDEs of image processing and their surprising applications','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/the-partial-differential-equations-of-image-processing-and-their-surprising-applications--214255.kjsp','J.-M. MOREL', 'image');
INSERT INTO "course" VALUES(6,'Reinforcement learning','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/reinforcement-learning-214281.kjsp','A. LAZARIC', 'learn');
INSERT INTO "course" VALUES(7,'Convex optimization and applications in machine learning','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/convex-optimization-and-applications-in-machine-learning-161994.kjsp','A. D''ASPREMONT', 'learn');
INSERT INTO "course" VALUES(8,'Introduction à l''imagerie numérique','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/introduction-a-l-imagerie-numerique-222029.kjsp','J. DELON,Y. GOUSSEAU', 'image');
INSERT INTO "course" VALUES(9,'Graphs in machine learning','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/graphs-in-machine-learning-267194.kjsp','M. VALKO', 'learn');
INSERT INTO "course" VALUES(10,'Optimization - Applications to image processing','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/optimization-applications-to-image-processing-164654.kjsp','S. LADJAL, M. NIKOLOVA', 'image');
INSERT INTO "course" VALUES(11,'Introduction to Deep Learning','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/introduction-to-deep-learning-165822.kjsp','I. KOKKINOS', 'learn');
INSERT INTO "course" VALUES(12,'Object recognition and computer vision','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/object-recognition-and-computer-vision-221990.kjsp','I. LAPTEV, J. PONCE, C. SCHMID, J. SIVIC', 'image');
INSERT INTO "course" VALUES(13,'Sub-pixel image processing','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/sub-pixel-image-processing-219041.kjsp','L. MOISAN', 'image');
INSERT INTO "course" VALUES(14,'3D computer vision','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/3d-computer-vision-214777.kjsp','R. MARLET, P. MONASSE', 'image');
INSERT INTO "course" VALUES(15,'Sparsity and compressed sensing','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/sparsity-and-compressed-sensing-214256.kjsp','G. PEYRE', 'learn');
INSERT INTO "course" VALUES(16,'Introduction to medical image analysis','http://www.math.ens-cachan.fr/version-francaise/formations/master-mva/contenus-/introduction-to-medical-image-analysis-218869.kjsp','H. DELINGETTE, X. PENNEC', 'image');

CREATE UNIQUE INDEX course_name ON course (name);

CREATE TABLE event (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  version INT,
  start TIME,
  end TIME,
  location VARCHAR
);

CREATE TABLE lesson (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  course INTEGER,
  date DATE,
  tp BOOL,
  uuid CHAR(32),
  event INTEGER,
  FOREIGN KEY (course) REFERENCES course (id)
  FOREIGN KEY (event) REFERENCES event (id)
);

CREATE UNIQUE INDEX lesson_index ON lesson (course, date, tp);

CREATE UNIQUE INDEX lesson_uuid ON lesson (uuid);

CREATE TABLE student (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  email VARCHAR,
  alert BOOL
);

CREATE UNIQUE INDEX student_email ON student (email);

CREATE TABLE student_courses (
  student INTEGER,
  course INTEGER,
  FOREIGN KEY (student) REFERENCES student (id)
  FOREIGN KEY (course) REFERENCES course (id)
);

CREATE UNIQUE INDEX student_courses_student ON student_courses (student, course);

COMMIT;
