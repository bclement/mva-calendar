import json
from icalendar import Calendar, Event
from icalendar.prop import vUri
from datetime import datetime, timedelta
from convert import course_info


def create_calendar(courses):
    with open('planning.json') as f:
        planning = json.load(f)
    cal = Calendar()
    cal.add('prodid', '-//Basile Clement//MVA Planning Parser//FR')
    cal.add('version', '2.0')
    for uid, course in planning.items():
        version = course['version']
        course = course['value']
        if course['name'] not in courses:
            continue
        event = Event()
        date = datetime.strptime(course['date'], '%Y-%m-%d')
        start = timedelta(**dict(zip(['hours', 'minutes', 'seconds'],
                                     [int(x) for x in course['start'].split(':')])))
        end = timedelta(**dict(zip(['hours', 'minutes', 'seconds'],
                                   [int(x) for x in course['end'].split(':')])))
        event.add('dtstart', date + start)
        event.add('dtend', date + end)
        event.add('summary', course['name'])
        event.add('location', course['location'])
        event['url'] = vUri(course_info[course['name']]['url'])
        event['uid'] = uid
        event['sequence'] = version
        cal.add_component(event)
    return cal


if __name__ == '__main__':
    with open('planning.ics', 'wb') as f:
        f.write(create_calendar({'Introduction to medical image analysis'}).to_ical())
