import json
from convert import build_planning

if __name__ == '__main__':
    try:
        new_planning = build_planning()
        with open('planning.json') as f:
            old_planning = json.load(f)
        if new_planning.keys() != old_planning.keys():
            raise Exception("A course was moved.")
        updated = []
        for k, new_course in new_planning:
            old_course = old_planning[k]
            if new_course == old_course['value']:
                continue
            updated.append(k)
        if len(updated) > 0:
            pass  # TODO: generate and send the mail
    except Exception:
        pass  # TODO: generate and send the mail
